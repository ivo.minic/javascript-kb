/**
 * All specific things regarding async functions.
 * Async called functions always return a promise. If it's return value is not a promise, it promisify it.
 * Await keyword stops execution only in async function it is called, but other functions in file/package are not interrupted.
 * Await is cleaner way to read value, than using then in promises.
 * Errors while using await keyword can be handled using try/catch block, or .catch(e => {}) method, since it returns a promise.
 *
 */

function getNumber42() {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(42), 2000);
  });
}

async function invoke() {
  const number42 = await getNumber42();
  console.log(number42);
}
invoke();

/** Two ways of creating same fetch call. */

function getRandomDogImage() {
  fetch("https://dog.ceo/api/breeds/image/random")
    .then((response) => response.json())
    .then((value) => console.log("Dog:", value.message));
}

async function getRandomDogImage() {
  let response = await fetch("https://dog.ceo/api/breeds/image/random");
  let value = await response.json();
  console.log("Dog async", value.message);
}

getRandomDogImage();

/** Sequential and parallel execution */

function printNumber1() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log("printNumber1 is done.");
      resolve(1);
    }, 1000);
  });
}

function printNumber2() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log("printNumber2 is done.");
      resolve(2);
    }, 1000);
  });
}

async function oneByOne() {
  const number1 = await printNumber1();
  const number2 = await printNumber2();
  console.log(number1, number2);
}
oneByOne();

async function inParallel() {
  const promise1 = printNumber1();
  const promise2 = printNumber2();
  const number1 = await promise1;
  const number2 = await promise2;
  //const [number1, number2] = [await promise1, await promise2]
  console.log(number1, number2);
  //console.log(await promise1, await promise2);
}
inParallel();
