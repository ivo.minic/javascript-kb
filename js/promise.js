/**
 * All specific things regarding promises.
 * Promise is a special JavaScript object that represents an eventual result of an asynchronous action.
 * It takes a callback and returns a promise. Promise has two internal properties: status and value.
 * Promise can be in pending status (then value is undefined), fulfilled (actual value) or rejected (no value).
 * Promise can take two arguments, both functions (resolve and rejects).
 * Method then() returns promise, so it can be chained with other then methods, or catch.
 * Method catch() is called if error is thrown, or promise is rejected.
 * Promise.resolve(value); returns promise, that can be chained with .then(). This is promisify of values.
 */

function calculateSquareNumber(number) {
  const promise = new Promise((resolve, reject) => {
    setTimeout(() => {
      if (typeof number !== "number") {
        reject(new Error("Number expected error."));
      }
      resolve(number * number);
    }, 1000);
  });
  return promise;
}

calculateSquareNumber(1)
  .then((value) => {
    console.log("Square value1: " + value);
    return calculateSquareNumber(2);
  })
  .then((value) => {
    console.log("Square value2: " + value);
    return calculateSquareNumber("asd");
  })
  .then((value) => {
    console.log("Square value3: " + value);
  })
  .catch((reason) => {
    console.log("Catch error", reason);
  });

/**
 * It is possible to execute promises in parallel. Promise.all([array of promises]) resolves when all promises are resolved.
 * It rejects, when first promise rejects. If all promises resolve, it returns an array of values.
 * Values have position in array as corresponding promises.
 * Promise.race([array of promises]) resolves when first promise resolves, and returns that value.
 */

function firstPromise() {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve("First response"), 8000);
  });
}

function secondPromise() {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve("Second response"), 5000);
  });
}

Promise.all([firstPromise(), secondPromise()]).then((value) => {
  console.log("Promise all values: " + value);
});

Promise.race([firstPromise(), secondPromise()]).then((value) => {
  console.log("Promise race values: " + value);
});

/**
 * Fetch returns response, and invoking .json() method returns promisified values.
 */
fetch("https://dummyjson.com/products/1")
  .then((res) => res.json())
  .then((json) => console.log("Fetch values", json))
  .catch((reason) => console.log("Error reason", reason));
